Installation
=================

Cinema is now available for Linux, via ``pip``. 

Linux
-----------

pip
^^^^^^^^

Cinema on PyPI is built following the *manylinux2014* specification,
which provides compatibility among Linux distributions and 
python versions. 

To install Cinema, run the following command on bash:

.. code-block:: python

    pip install neutron-cinema
