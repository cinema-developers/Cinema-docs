Welcome to Cinema's documentation!
===================================

**Cinema** (China Spallation Neutron Source Monte Carlo System) uses
`NCrystal <https://github.com/mctools/ncrystal/>`_ as the backend for the scattering physics,
`VecGeom <https://gitlab.cern.ch/VecGeom/VecGeom>`_ for the geometry modelling and
`pyvista <https://www.pyvista.org/>`_ for the visualisation.
The cross section generators for crystalline materials *PiXiu* and liquids *Tak* will be included in the future releases.

.. Check out the :doc:`usage` section for further information, including
.. how to :ref:`installation` the project.

.. note::

   This project is under active development.

.. toctree::
   :maxdepth: 1

   installation
   tutorial/index
   theory/index
   usersmanual/index
   publications
   references
