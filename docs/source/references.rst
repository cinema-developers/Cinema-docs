References
=================

Physics
-----------
#. X.-X. Cai and T. Kittelmann, NCrystal: A library for thermal neutron
   transport, Computer Physics Communications 246 (2020) 106851,
   https://doi.org/10.1016/j.cpc.2019.07.015

Geometry
-------------
#. APOSTOLAKIS, John, et al. Vectorising the detector geometry 
   to optimise particle transport. 
   In: Journal of Physics: Conference Series. 
   IOP Publishing, 2014. p. 052038, 
   https://doi.org/10.1088/1742-6596/513/5/052038

Visualisation
-------------
#. Sullivan and Kaszynski, (2019). 
   PyVista: 3D plotting and mesh analysis 
   through a streamlined interface for the Visualization Toolkit (VTK). 
   Journal of Open Source Software, 4(37), 1450, 
   https://doi.org/10.21105/joss.01450
